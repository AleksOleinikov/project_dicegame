using System.Collections;
using UnityEngine;

namespace DiceGame
{
	public class MoveController : MonoBehaviour
	{
		[SerializeField] private float moveTime = .2f;

		public void SetPosition(Vector3 position)
		{
			transform.position = position;
		}

		public IEnumerator MoveSmooth(Vector3 position)
		{
			var startPos = transform.position;
			for (float i = 0; i < 1; i += Time.deltaTime / moveTime)
			{
				transform.position = Vector3.Lerp(startPos, position, i);
				yield return null;
			}

			SetPosition(position);
		}
	}
}