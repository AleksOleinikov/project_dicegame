using UnityEngine;

namespace DiceGame
{
	public class Player : MonoBehaviour
	{
		[SerializeField] private MoveController moveController;
		[SerializeField] private PlayerUIController playerUI;
		[SerializeField] private Animator hitAnimator;
		[SerializeField] private ParticleSystem hitPS;
		[SerializeField] private PlayerAnimationController animationController;

		[Header("Visual")]
		[SerializeField] private Transform visualRoot;

		public MoveController MoveController => moveController;

		public PlayerUIController PlayerUI => playerUI;

		public PlayerAnimationController AnimationController => animationController;

		public void LookAt(Vector3 position)
		{
			visualRoot.LookAt(new Vector3(position.x, 0, position.z));
		}

		public void GetHit()
		{
			hitAnimator.SetTrigger("getHit");
			hitPS.Play();
		}
	}
}