using System;
using System.Collections.Generic;

namespace DiceGame
{
	[Serializable]
	public class EnemiesToRange
	{
		public int MinIndex;
		public int MaxIndex;
		public List<EnemyToWeight> Enemies;
	}
}