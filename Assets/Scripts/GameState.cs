namespace DiceGame
{
	public enum GameState
	{
		Game = 0,
		Treasure = 1,
		Battle = 2,
		GameOver = 3
	}
}