using Cinemachine;
using UnityEngine;
using UnityEngine.UI;

namespace DiceGame
{
	[RequireComponent(typeof(CinemachineVirtualCamera))]
	public class VirtualCameraAspectRationFitter : MonoBehaviour
	{
		[SerializeField] private Vector2 referenceResolution;
		

		private void Awake()
		{
			Fit();
		}

		private void Fit()
		{
			var camera = GetComponent<CinemachineVirtualCamera>();
			var scaleFactor = Mathf.Max(Screen.width / referenceResolution.x, Screen.height / referenceResolution.y);
			camera.m_Lens.OrthographicSize *= scaleFactor;
		}
	}
}