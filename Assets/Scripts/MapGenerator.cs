using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DiceGame
{
	public class MapGenerator : SingletonController<MapGenerator>
	{
		[SerializeField] private int minSameSideFieldsInRow = 3;
		[SerializeField] private int maxSameSideFieldsInRow = 8;
		[SerializeField] private Field fieldPrefab;
		[SerializeField] private float fieldSideSize;

		[Header("Pattern")]
		[SerializeField] private List<MapPattern> patterns;

		public Queue<Field> ActiveFieldsQueue => activeFieldsQueue;
		public Field CurrentField { get; private set; }

		private Stack<Field> fieldsPool;
		private Queue<Field> activeFieldsQueue;
		private Vector3 lastFieldPosition = Vector3.zero;
		private bool isFirstField = true;
		private int fieldsInRow;
		private bool isLastFieldRight;

		private DiceController dc;
		private GameManager gm;

		private void Awake()
		{
			base.Awake();
			fieldsPool = new Stack<Field>();
			activeFieldsQueue = new Queue<Field>();
		}

		private void Start()
		{
			dc = DiceController.Instance;
			gm = GameManager.Instance;
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.R))
			{
				Reset();
			}
		}

		public void Reset()
		{
			StartCoroutine(ResetMap());
		}

		private IEnumerator ResetMap()
		{
			lastFieldPosition = Vector3.zero;
			isFirstField = true;
			if (CurrentField != null) StartCoroutine(CurrentField.Hide(0));

			var fieldsCount = activeFieldsQueue.Count;
			if (fieldsCount > 0)
			{
				for (int i = 0; i < fieldsCount - 1; i++)
				{
					StartCoroutine(activeFieldsQueue.Dequeue().Hide(i + 1));
				}

				yield return StartCoroutine(activeFieldsQueue.Dequeue().Hide(fieldsCount));
			}

			GenerateMap();
			CurrentField = activeFieldsQueue.Dequeue();
		}

		public void ChangeCurrentField(int index)
		{
			if (CurrentField != null) StartCoroutine(CurrentField.Hide(index));

			CurrentField = activeFieldsQueue.Dequeue();
		}

		private void GenerateMap()
		{
			var minFields = GetMinRequiredFieldsCount();
			var patternsToGenerate = GetPatternsToGenerate(minFields);
			var index = 0;
			foreach (var pattern in patternsToGenerate)
			{
				foreach (var type in pattern.FieldTypes)
				{
					var field = GetFromPool();
					field.transform.position = GetFieldPosition();
					field.gameObject.SetActive(true);
					field.SetField(type, index);
					index++;
				}
			}
		}

		private List<MapPattern> GetPatternsToGenerate(int requiredFields)
		{
			var result = new List<MapPattern>();
			var fieldsInPatterns = 0;
			while (fieldsInPatterns < requiredFields)
			{
				var index = Random.Range(0, patterns.Count);
				var pattern = patterns[index];
				fieldsInPatterns += pattern.FieldTypes.Count;
				result.Add(pattern);
			}

			return result;
		}

		private Vector3 GetFieldPosition()
		{
			if (isFirstField)
			{
				isFirstField = false;
				lastFieldPosition = Vector3.zero;
				return lastFieldPosition;
			}

			var isRight = fieldsInRow < minSameSideFieldsInRow ? isLastFieldRight :
				fieldsInRow > maxSameSideFieldsInRow ? !isLastFieldRight : Random.Range(0, 2) == 1;
			if (isLastFieldRight != isRight)
			{
				fieldsInRow = 0;
			}
			else
			{
				fieldsInRow++;
			}

			isLastFieldRight = isRight;

			var offset = isRight ? Vector3.forward * fieldSideSize : Vector3.left * fieldSideSize;
			lastFieldPosition += offset;
			return lastFieldPosition;
		}

		public void UpdateMap()
		{
			if (!ShouldGenerateFields()) return;

			GenerateMap();
		}

		private bool ShouldGenerateFields()
		{
			return activeFieldsQueue.Count < GetMinRequiredFieldsCount();
		}

		private int GetMinRequiredFieldsCount()
		{
			return dc.ActiveDicesCount * 6 + 1;
		}

		private void InstantiateField()
		{
			var instance = Instantiate(fieldPrefab, Vector3.zero, Quaternion.identity, transform);
			AddToPool(instance);
		}

		public void AddToPool(Field field)
		{
			field.gameObject.SetActive(false);
			fieldsPool.Push(field);
		}

		private Field GetFromPool()
		{
			if (fieldsPool.Count <= 0)
			{
				InstantiateField();
			}

			var field = fieldsPool.Pop();
			activeFieldsQueue.Enqueue(field);
			return field;
		}
	}
}