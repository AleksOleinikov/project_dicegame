using UnityEngine;

namespace DiceGame
{
	public class Chest : MonoBehaviour
	{
		[SerializeField] private Animator animator;
		[SerializeField] private Transform chestResourcesTransform;

		public Transform ChestResourcesTransform => chestResourcesTransform;

		private static readonly int Open1 = Animator.StringToHash("Open");


		public void Open()
		{
			animator.SetTrigger(Open1);
		}
	}
}