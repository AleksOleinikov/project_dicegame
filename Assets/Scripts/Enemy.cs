using System.Collections;
using UnityEngine;

namespace DiceGame
{
	public class Enemy : MonoBehaviour
	{
		[SerializeField] private MoveController moveController;
		[SerializeField] private EnemyUIController viewRoot;
		[SerializeField] private ParticleSystem hitPS;
		[SerializeField] private Animator brokenShield;
		[SerializeField] private ResourceUIAnimator animatorUI;

		[SerializeField] private float smallScale = .5f;

		public MoveController MoveController => moveController;

		public void SetParameters(int defence, int currentHealth, bool isHit = false)
		{
			gameObject.transform.localScale = Vector3.one;
			viewRoot.gameObject.SetActive(true);
			if (isHit)
			{
				hitPS.gameObject.SetActive(true);
				hitPS.Play();
				brokenShield.SetTrigger("getHit");
				StartCoroutine(animatorUI.Animate(false, currentHealth));
				return;
			}

			viewRoot.UpdateView(defence, currentHealth);
		}

		private IEnumerator DoHitWithDelay(int defence, int maxHealth, int currentHealth)
		{
			yield return new WaitForSeconds(1);

			viewRoot.UpdateView(defence, currentHealth);
		}

		private void OnEnable()
		{
			Reset();
		}

		private void Reset()
		{
			viewRoot.gameObject.SetActive(false);
			gameObject.transform.localScale = Vector3.one * smallScale;
		}
	}
}