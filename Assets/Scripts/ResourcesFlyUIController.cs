using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DiceGame
{
	public class ResourcesFlyUIController : SingletonController<ResourcesFlyUIController>
	{
		[SerializeField] private Canvas canvas;
		[SerializeField] private Camera mainCamera;
		[SerializeField] private FlyResourceUIView prefab;
		[SerializeField] private Vector3 minMidPositionDeviation;
		[SerializeField] private Vector3 maxMidPositionDeviation;
		[SerializeField] private float flyLength = 0.5f;
		[SerializeField] private float sendDelay = 0.1f;
		[SerializeField] private List<ResourceToView> resourceToViews;


		private RectTransform canvasRect;
		private List<FlyResourceUIView> resources;
		private WaitForSeconds waitSendDelay;

		private void Start()
		{
			canvasRect = canvas.GetComponent<RectTransform>();
			waitSendDelay = new WaitForSeconds(sendDelay);
			foreach (var rtv in resourceToViews)
			{
				var targetSlotScreenPosition =
					RectTransformUtility.WorldToScreenPoint(mainCamera, rtv.targetTransform.position);
				RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect, targetSlotScreenPosition,
					mainCamera, out var resUIPos);
				rtv.TargetPosition = resUIPos;
			}
		}

		public IEnumerator SendResources(List<ResourceToValue> rewards, Vector3 treasurePoint)
		{
			resources = new List<FlyResourceUIView>();
			var startUIPos = RectTransformUtility.WorldToScreenPoint(mainCamera, treasurePoint);
			startUIPos = new Vector2(startUIPos.x - Screen.width / 2, startUIPos.y - Screen.height / 2);

			foreach (var reward in rewards)
			{
				var rtv = GetResourceToView(reward.Resource);

				Vector3 midPosition = startUIPos + (rtv.TargetPosition - startUIPos) * 0.5f;

				for (int i = 0; i < reward.Value; i++)
				{
					midPosition += Vector3.Lerp(minMidPositionDeviation, maxMidPositionDeviation, Random.Range(0f, 1f));
					var instance = Instantiate(prefab, transform);
					instance.UpdateView(reward.Resource, rtv.targetColor, startUIPos, midPosition, midPosition,
						rtv.TargetPosition, flyLength);

					resources.Add(instance);

					yield return waitSendDelay;
				}
			}

			while (resources.Any(x => x.IsFlying))
			{
				yield return null;
			}

			resources.ForEach(x => Destroy(x.gameObject));
		}

		private ResourceToView GetResourceToView(Resource resource)
		{
			return resourceToViews.Find(x => x.targetResource == resource);
		}
	}

	[Serializable]
	public class ResourceToView
	{
		public Resource targetResource;
		public Transform targetTransform;
		public Gradient targetColor;

		public Vector2 TargetPosition { get; set; }
	}
}