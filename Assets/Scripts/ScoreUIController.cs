using TMPro;
using UnityEngine;

namespace DiceGame
{
	public class ScoreUIController : MonoBehaviour
	{
		[SerializeField] private TextMeshProUGUI currentScoreTXT;
		[SerializeField] private TextMeshProUGUI highScoreTXT;

		private GameManager gm;


		private void Start()
		{
			gm = GameManager.Instance;

			gm.OnScoreUpdate.AddListener(UpdateView);
			UpdateView();
		}

		private void UpdateView()
		{
			currentScoreTXT.text = $"Path: {gm.CurrentFieldIndex}";
			highScoreTXT.text = $"Highscore: {gm.HighScore}";
		}
	}
}