using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DiceGame
{
	public class Dice : MonoBehaviour
	{
		private const string SHADER_PARAM_NAME = "_Cutoff";


		[SerializeField] private List<DiceSideProvider> sides;
		[SerializeField] private float rollEndDelay = 2f;
		[SerializeField] private Rigidbody rb;
		[SerializeField] private float magnetizeDelay = 1.5f;
		[SerializeField] private float magnetizeForce = 5;

		[SerializeField] private AnimationCurve showCurve;
		[SerializeField] private float showLength;
		[SerializeField] private AnimationCurve hideCurve;
		[SerializeField] private float hideLength;
		[SerializeField] private float hideDelay = .5f;
		[SerializeField] private Renderer renderer;


		public Rigidbody Rigidbody => rb;

		private bool isRolling;
		private float rollStart;
		private float lastMagnetize;
		private Material diceMaterial;

		private void Awake()
		{
			diceMaterial = renderer.material;
		}

		private void Update()
		{
			if (!isRolling) return;

			MagnetizeToFloor();

			if (!IsDiceSleeping()) return;

			isRolling = false;
			GetResult();
			StartCoroutine(FadeInOut(hideCurve, hideLength, 0, 1, true, hideDelay));
		}

		public void StarRolling()
		{
			lastMagnetize = Time.timeSinceLevelLoad;
			isRolling = true;
			rollStart = Time.timeSinceLevelLoad;
			StopAllCoroutines();
			StartCoroutine(FadeInOut(showCurve, showLength, 1, 0));
		}

		private bool IsDiceSleeping()
		{
			return rollStart + rollEndDelay < Time.timeSinceLevelLoad && rb.velocity == Vector3.zero;
		}

		private void GetResult()
		{
			DiceController.Instance.AddResult((int) GetHighestSide().Side);
		}

		private DiceSideProvider GetHighestSide()
		{
			DiceSideProvider highestSide = null;
			foreach (var side in sides)
			{
				if (highestSide != null && highestSide.Position.y > side.Position.y) continue;

				highestSide = side;
			}

			return highestSide;
		}


		private IEnumerator FadeInOut(AnimationCurve curve, float length, float startValue, float targetValue,
			bool disable = false, float delay = 0)
		{
			if (delay > 0)
			{
				yield return new WaitForSeconds(delay);
			}

			for (float i = 0; i < 1; i += Time.deltaTime / length)
			{
				var c = curve.Evaluate(i);
				var cutoff = Mathf.Lerp(startValue, targetValue, c);
				diceMaterial.SetFloat(SHADER_PARAM_NAME, cutoff);
				yield return null;
			}

			diceMaterial.SetFloat(SHADER_PARAM_NAME, targetValue);
			if (disable) gameObject.SetActive(false);
		}

		private void MagnetizeToFloor()
		{
			if (lastMagnetize + magnetizeDelay > Time.timeSinceLevelLoad) return;

			lastMagnetize = Time.timeSinceLevelLoad;
			var side = GetHighestSide();
			var targetDirection = -(side.Position - transform.position).normalized;
			rb.AddForce(targetDirection * magnetizeForce);
		}
	}
}