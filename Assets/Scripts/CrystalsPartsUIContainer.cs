using UnityEngine;

namespace DiceGame
{
	public class CrystalsPartsUIContainer : MonoBehaviour
	{
		[SerializeField] private GameObject template;
		[SerializeField] private RectTransform rootView;

		public void UpdateView(int max, int stepSize)
		{
			var partsCount = max / stepSize;
			var width = rootView.rect.width;
			var rectStep = width * (stepSize / (float) max);
			var posX = 0f;
			for (int i = 0; i < partsCount; i++)
			{
				var instance = Instantiate(template, rootView.transform);
				var rect = instance.GetComponent<RectTransform>();
				posX += rectStep;
				rect.anchoredPosition = new Vector2(posX, 0);
			}
		}
	}
}