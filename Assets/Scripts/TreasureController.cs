using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DiceGame
{
	public class TreasureController : SingletonController<TreasureController>
	{
		[SerializeField] private Chest chestPrefab;
		[SerializeField] private float openDelay = 2f;
		[SerializeField] private float flyDelay;
		[SerializeField] private float hideDelay;

		private Chest chest;

		private void Awake()
		{
			base.Awake();
			chest = Instantiate(chestPrefab);
			chest.gameObject.SetActive(false);
		}

		public IEnumerator OpeningCoroutineWithoutTreasure(Vector3 resourcesPos, List<ResourceToValue> resourceToValues)
		{
			ResourcesFlyUIController.Instance.StopAllCoroutines();

			yield return
				StartCoroutine(ResourcesFlyUIController.Instance.SendResources(resourceToValues, resourcesPos));
		}

		public IEnumerator OpeningCoroutine(Vector3 chestPos, List<ResourceToValue> resourceToValues)
		{
			chest.transform.position = chestPos;
			chest.gameObject.SetActive(true);
			yield return new WaitForSeconds(openDelay);

			chest.Open();

			yield return new WaitForSeconds(flyDelay);

			ResourcesFlyUIController.Instance.StopAllCoroutines();

			yield return StartCoroutine(ResourcesFlyUIController.Instance.SendResources(resourceToValues,
				chest.ChestResourcesTransform.position));

			yield return new WaitForSeconds(hideDelay);
			chest.gameObject.SetActive(false);
		}
	}
}