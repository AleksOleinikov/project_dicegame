using System.Collections;
using UnityEngine;

namespace DiceGame
{
	public class PlayerUIController : MonoBehaviour
	{
		[SerializeField] private UIView victoryRoot;
		[SerializeField] private UIView defeatRoot;
		[SerializeField] private float hideDelay;

		[SerializeField] private GameObject battlehealth;

		private WaitForSeconds waitToHide;

		private void Awake()
		{
			waitToHide = new WaitForSeconds(hideDelay);
			battlehealth.gameObject.SetActive(false);
		}

		public void ShowBattleHealth()
		{
			battlehealth.gameObject.SetActive(true);
		}

		public void OnBattleEnd(bool isVictory)
		{
			battlehealth.gameObject.SetActive(false);

			if (isVictory)
			{
				victoryRoot.Show();
			}
			else
			{
				defeatRoot.Show();
			}

			StartCoroutine(HideAfterDelayCoroutine());
		}

		private IEnumerator HideAfterDelayCoroutine()
		{
			yield return waitToHide;

			victoryRoot.Hide();
			defeatRoot.Hide();
		}
	}
}