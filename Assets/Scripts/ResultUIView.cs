using TMPro;
using UnityEngine;

namespace DiceGame
{
	public class ResultUIView : MonoBehaviour
	{
		[SerializeField] private TextMeshProUGUI resultTXT;

		public void UpdateView(int result)
		{
			resultTXT.text = result.ToString();
		}
	}
}