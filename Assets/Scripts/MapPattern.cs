using System;
using System.Collections.Generic;

namespace DiceGame
{
	[Serializable]
	public class MapPattern
	{
		public List<FieldType> FieldTypes;
	}
}