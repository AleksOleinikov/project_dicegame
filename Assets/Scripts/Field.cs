using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DiceGame
{
	public class Field : MonoBehaviour
	{
		[SerializeField] private List<FieldToVisual> visuals;
		[SerializeField] private GameObject visualsRoot;
		[SerializeField] private float lowPos = -20f;
		[SerializeField] private float showDelay = .2f;
		[SerializeField] private float showLength = 2;
		[SerializeField] private AnimationCurve showCurve;
		[SerializeField] private float hideDelay = 2;
		[SerializeField] private AnimationCurve hideCurve;

		[Header("Battle")]
		[SerializeField] private Transform playerBattlePos;
		[SerializeField] private Transform enemyBattlePos;

		[Header("Treasure")]
		[SerializeField] private Transform playerTreasurePos;
		[SerializeField] private Transform treasurePos;

		public Vector3 PlayerBattlePos => playerBattlePos.position;
		public Vector3 EnemyBattlePos => enemyBattlePos.position;
		public Vector3 PlayerTreasurePos => playerTreasurePos.position;
		public Vector3 TreasurePos => treasurePos.position;

		private FieldType currentType;

		public FieldType CurrentType
		{
			get => currentType;
			set => currentType = value;
		}

		public void SetField(FieldType type, int index)
		{
			currentType = type;
			foreach (var visual in visuals)
			{
				visual.Visual.SetActive(visual.Type == type);
			}

			StartCoroutine(Move(showCurve, showLength, lowPos, 0, showDelay * index));
		}

		public IEnumerator Hide(int index)
		{
			yield return new WaitForSeconds(hideDelay);

			yield return StartCoroutine(Move(hideCurve, showLength, 0, lowPos, showDelay * index));

			MapGenerator.Instance.AddToPool(this);
		}

		private IEnumerator Move(AnimationCurve curve, float length, float startYPos, float endYPos, float delay)
		{
			visualsRoot.transform.position = new Vector3(visualsRoot.transform.position.x, startYPos,
				visualsRoot.transform.position.z);
			yield return new WaitForSeconds(delay);

			for (float i = 0; i < 1; i += Time.deltaTime / length)
			{
				var c = curve.Evaluate(i);
				var posY = Mathf.Lerp(startYPos, endYPos, c);
				visualsRoot.transform.position = new Vector3(visualsRoot.transform.position.x, posY,
					visualsRoot.transform.position.z);
				yield return null;
			}

			visualsRoot.transform.position = new Vector3(visualsRoot.transform.position.x, endYPos,
				visualsRoot.transform.position.z);
		}
	}

	[Serializable]
	public class FieldToVisual
	{
		public FieldType Type;
		public GameObject Visual;
	}

	public enum FieldType
	{
		Empty = 0,
		Reward = 1,
		Enemy = 2,
		Rolls = 3,
		Crystals = 4
	}
}