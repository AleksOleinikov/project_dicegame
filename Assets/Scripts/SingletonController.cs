using UnityEngine;

namespace DiceGame
{
	public class SingletonController<T> : MonoBehaviour where T : MonoBehaviour
	{
		public static T Instance;

		protected void Awake()
		{
			if (Instance == null)
			{
				Instance = this as T;
			}
			else
			{
				Debug.LogError($"Singleton Controller type: {typeof(T)} Already Exists");
				Destroy(this);
			}
		}
	}
}