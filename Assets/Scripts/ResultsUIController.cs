using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DiceGame
{
	public class ResultsUIController : MonoBehaviour
	{
		[SerializeField] private ResultUIView finalResultView;
		[SerializeField] private UIView finalResultUIView;
		[SerializeField] private ResultsUIView resultsContainer;
		[SerializeField] private UIView resultsUIView;
		[SerializeField] private float hideDelay = 3;

		private WaitForSeconds wait;

		private void Awake()
		{
			wait = new WaitForSeconds(hideDelay);
			finalResultUIView.gameObject.SetActive(false);
			resultsUIView.gameObject.SetActive(false);
		}

		public void UpdateView(List<int> results, int finalResult)
		{
			resultsContainer.UpdateView(results);
			finalResultView.UpdateView(finalResult);
			finalResultUIView.gameObject.SetActive(true);
			resultsUIView.gameObject.SetActive(true);
			finalResultUIView.Show();
			resultsUIView.Show();
			StopAllCoroutines();
			StartCoroutine(HideAfterDelay());
		}

		private IEnumerator HideAfterDelay()
		{
			yield return wait;
			Hide();
		}

		public void Hide()
		{
			finalResultUIView.Hide();
			resultsUIView.Hide();
		}
	}
}