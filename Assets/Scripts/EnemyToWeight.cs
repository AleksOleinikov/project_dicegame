using System;

namespace DiceGame
{
	[Serializable]
	public class EnemyToWeight
	{
		public EnemyPreset Enemy;
		public float Weight;
	}
}