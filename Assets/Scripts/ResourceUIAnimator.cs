using System.Collections;
using TMPro;
using UnityEngine;

namespace DiceGame
{
	public class ResourceUIAnimator : MonoBehaviour
	{
		[SerializeField] private Transform root;
		[SerializeField] private float animationLength = .2f;
		[SerializeField] private float animationChangePercent = .7f;
		[SerializeField] private AnimationCurve sizeCurve;
		[SerializeField] private Color positiveColor;
		[SerializeField] private Color negativeColor;
		[SerializeField] private TextMeshProUGUI text;

		private Color defaultColor;

		private void Awake()
		{
			defaultColor = text?.color ?? Color.white;
		}

		public IEnumerator Animate(bool isPositive = true, int value = 0, string mask = "{0}")
		{
			bool valueChanged = false;
			for (float i = 0; i < 1; i += Time.deltaTime / animationLength)
			{
				var s = sizeCurve.Evaluate(i);
				root.localScale = Vector3.one * s;
				if (text != null && !valueChanged && i >= animationChangePercent)
				{
					valueChanged = true;
					text.text = string.Format(mask, value.ToString());
					text.color = isPositive ? positiveColor : negativeColor;
				}

				yield return null;
			}

			Reset();
		}

		private void Reset()
		{
			root.localScale = Vector3.one;
			if (text == null) return;

			text.color = defaultColor;
		}
	}
}