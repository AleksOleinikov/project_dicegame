using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DiceGame
{
	public abstract class ContainerUIView<T> : MonoBehaviour where T : MonoBehaviour
	{
		[SerializeField] private RectTransform layoutRectTransform;
		public Transform root;
		public T template;

		public readonly List<T> views = new List<T>();
		private List<T> pool;

		protected virtual T InstantiateItem()
		{
			var item = Instantiate(template, root.transform);
			item.gameObject.SetActive(true);
			return item;
		}

		public List<T> GetOrCreateItemViews(int maxCapacity, bool reverseOrder = false)
		{
			ResizePool(maxCapacity);

			views.Clear();
			for (var i = 0; i < maxCapacity; ++i)
			{
				var view = pool[i];
				view.gameObject.SetActive(true);
				if (reverseOrder)
				{
					views.Insert(0, view);
				}
				else
				{
					views.Add(view);
				}
			}

			return views;
		}

		private void ResizePool(int capacity)
		{
			if (pool == null)
			{
				pool = new List<T>();
				root.GetComponentsInChildren(true, pool);
			}

			for (var i = pool.Count; i < capacity; ++i)
			{
				pool.Add(InstantiateItem());
			}

			for (var i = capacity; i < pool.Count; ++i)
			{
				pool[i].gameObject.SetActive(false);
			}
		}

		protected void ForceRebuildContainer()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(layoutRectTransform);
		}
	}
}