using UnityEngine;
using UnityEngine.UI;

namespace DiceGame
{
	public class CrystalsUIController : MonoBehaviour
	{
		[SerializeField] private CrystalsPartsUIContainer partsView;
		[SerializeField] private Image progressBar;
		[SerializeField] private Resource targetResource;
		[SerializeField] private ResourceUIAnimator animator;

		private GameManager gm;
		private int maxCrystals;

		private void Start()
		{
			gm = GameManager.Instance;
			gm.OnResoureValueChange.AddListener(OnValueChange);
			maxCrystals = gm.MaxCrystals;
			partsView.UpdateView(maxCrystals, gm.CrystalsStepSize);
		}

		private void OnValueChange(Resource resource, int prevValue, int value)
		{
			if (!isActiveAndEnabled || resource != targetResource) return;

			StartCoroutine(animator.Animate());
			UpdateView();
		}

		private void UpdateView()
		{
			progressBar.fillAmount = gm.CurrentCrystals / (float) maxCrystals;
		}
	}
}