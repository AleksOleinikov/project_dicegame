using UnityEngine;
using UnityEngine.UI;

namespace DiceGame
{
	public class HUDUIController : MonoBehaviour
	{
		[SerializeField] private Button rollButton;
		[SerializeField] private GameObject root;

		private void Start()
		{
			rollButton.onClick.AddListener(Roll);
			GameManager.Instance.OnStateChange.AddListener(x => UpdateView());
			UpdateView();
		}

		private void UpdateView()
		{
			root.SetActive(GameManager.Instance.CurrentState != GameState.GameOver);
		}

		private void Roll()
		{
			GameManager.Instance.RollDices();
		}
	}
}