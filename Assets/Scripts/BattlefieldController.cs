using System.Collections;
using UnityEngine;

namespace DiceGame
{
	public class BattlefieldController : MonoBehaviour
	{
		private const string SHADER_PARAM_NAME = "_Cutoff";

		[SerializeField] private Renderer renderer;
		[SerializeField] private AnimationCurve showCurve;
		[SerializeField] private float showLength;
		[SerializeField] private AnimationCurve hideCurve;
		[SerializeField] private float hideLength;

		private Material material;

		private void Awake()
		{
			material = renderer.material;
		}

		public void PlaceBattlefield(Vector3 position)
		{
			gameObject.SetActive(true);
			transform.position = position;
			StartCoroutine(FadeInOut(showCurve, showLength, 1, 0));
		}

		public void HideBattleField()
		{
			StartCoroutine(FadeInOut(hideCurve, hideLength, 0, 1, true));
		}

		private IEnumerator FadeInOut(AnimationCurve curve, float length, float startValue, float targetValue,
			bool disable = false, float delay = 0)
		{
			if (delay > 0)
			{
				yield return new WaitForSeconds(delay);
			}

			for (float i = 0; i < 1; i += Time.deltaTime / length)
			{
				var c = curve.Evaluate(i);
				var cutoff = Mathf.Lerp(startValue, targetValue, c);
				material.SetFloat(SHADER_PARAM_NAME, cutoff);
				yield return null;
			}

			material.SetFloat(SHADER_PARAM_NAME, targetValue);
			if (disable) gameObject.SetActive(false);
		}
	}
}