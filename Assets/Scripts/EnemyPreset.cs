using UnityEngine;

namespace DiceGame
{
	[CreateAssetMenu(menuName = "Dice Game/Enemy Preset")]
	public class EnemyPreset : ScriptableObject
	{
		public int MaxHealth;
		public int Difficulty;
		public Enemy PrefabTemplate;
		public Treasure Treasure;
	}
}