using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace DiceGame
{
	public class FlyResourceUIView : MonoBehaviour
	{
		[SerializeField] private Image icon;
		[SerializeField] private TrailRenderer trail;
		[SerializeField] private AnimationCurve sizeCurve;


		public bool IsFlying { get; set; }

		private RectTransform rectTransform;

		private Resource targetResource;

		private void Awake()
		{
			rectTransform = GetComponent<RectTransform>();
		}

		public void UpdateView(Resource resource, Gradient trailColor, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3,
			float flyLength)
		{
			targetResource = resource;
			icon.sprite = resource.Icon;
			trail.colorGradient = trailColor;
			StopAllCoroutines();
			StartCoroutine(Fly(p0, p1, p2, p3, flyLength));
		}

		private IEnumerator Fly(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float flyLength)
		{
			IsFlying = true;
			for (float i = 0; i < 1; i += Time.deltaTime / flyLength)
			{
				var size = sizeCurve.Evaluate(i);
				var pos = CalculateCubicBezierPoint(i, p0, p1, p2, p3);
				rectTransform.localScale = Vector3.one * size;
				rectTransform.anchoredPosition = pos;
				yield return null;
			}

			rectTransform.anchoredPosition = p3;
			rectTransform.localScale = Vector3.one;

			icon.gameObject.SetActive(false);
			IsFlying = false;
			GameManager.Instance.AddResource(targetResource, 1);
		}

		private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
		{
			float u = 1 - t;
			float tt = t * t;
			float uu = u * u;
			float uuu = uu * u;
			float ttt = tt * t;

			Vector3 p = uuu * p0;
			p += 3 * uu * t * p1;
			p += 3 * u * tt * p2;
			p += ttt * p3;

			return p;
		}
	}
}