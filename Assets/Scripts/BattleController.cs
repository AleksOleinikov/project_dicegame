using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DiceGame
{
	public class BattleController : SingletonController<BattleController>
	{
		[SerializeField] private List<EnemiesToRange> enemiesToRanges;
		[SerializeField] private float hitDelay;

		private int currentHealth;
		private Enemy currentEnemy;
		private Field currentField;
		private EnemyPreset currentPreset;

		private GameManager gm;

		private WaitForSeconds waitHit;

		public void StartBattle()
		{
			if (gm == null)
			{
				gm = GameManager.Instance;
			}

			waitHit = new WaitForSeconds(hitDelay);

			currentField = MapGenerator.Instance.CurrentField;
			currentPreset = GetEnemyPreset();
			currentEnemy = Instantiate(currentPreset.PrefabTemplate);
			currentEnemy.transform.position = currentField.EnemyBattlePos;

			currentHealth = currentPreset.MaxHealth;
			StartCoroutine(gm.Player.MoveController.MoveSmooth(currentField.PlayerBattlePos));
			gm.Player.PlayerUI.ShowBattleHealth();
			gm.Player.LookAt(currentField.EnemyBattlePos);
			currentEnemy.SetParameters(currentPreset.Difficulty, currentHealth);
		}

		public void DoBattle(int result)
		{
			if (result >= currentPreset.Difficulty)
			{
				currentHealth--;

				gm.Player.AnimationController.DoAttack();
				StartCoroutine(GetHitWithDelay());
			}
			else
			{
				gm.CurrentHearts--;
				gm.Player.GetHit();
			}

			var playerIsDead = gm.CurrentHearts <= 0;
			var enemyIsDead = currentHealth <= 0;
			if (!playerIsDead && !enemyIsDead)
			{
				gm.UnblockRolls();
				return;
			}

			StartCoroutine(EndBattle(enemyIsDead));
		}

		private IEnumerator GetHitWithDelay()
		{
			yield return waitHit;

			currentEnemy.SetParameters(currentPreset.Difficulty, currentHealth, true);
		}

		private IEnumerator EndBattle(bool isVictory)
		{
			yield return new WaitForSeconds(1f);

			currentEnemy.gameObject.SetActive(false);
			Destroy(currentEnemy);

			StartCoroutine(gm.OnBattleEnd(isVictory, currentPreset.Treasure));
		}

		private EnemyPreset GetEnemyPreset()
		{
			var enemyRange = enemiesToRanges.First(x =>
				x.MinIndex <= gm.CurrentFieldIndex && x.MaxIndex >= gm.CurrentFieldIndex);
			var dropSum = enemyRange.Enemies.Sum(x => x.Weight);
			var rnd = Random.Range(0, dropSum);
			var result = 0f;
			foreach (var enemy in enemyRange.Enemies)
			{
				result += enemy.Weight;
				if (result < rnd) continue;

				return enemy.Enemy;
			}

			return enemyRange.Enemies[0].Enemy;
		}
	}
}