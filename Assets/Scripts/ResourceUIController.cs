using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DiceGame
{
	public class ResourceUIController : MonoBehaviour
	{
		[SerializeField] private Resource targetResource;
		[SerializeField] private Image icon;
		[SerializeField] private TextMeshProUGUI valueTXT;
		[SerializeField] private ResourceUIAnimator animator;
		[SerializeField] private GameObject animatorElement;
		[SerializeField] private string mask = "{0}";

		private void Start()
		{
			icon.sprite = targetResource.Icon;
			GameManager.Instance.OnResoureValueChange.AddListener(OnValueChange);

			SetCurrentValue();
		}

		private void OnEnable()
		{
			SetCurrentValue();
		}

		private void SetCurrentValue()
		{
			var currentValue = GameManager.Instance?.GetCurrentResourceCount(targetResource) ?? 0;
			UpdateView(currentValue, currentValue);
		}

		private void OnValueChange(Resource resource, int prevValue, int value)
		{
			if (!isActiveAndEnabled || resource != targetResource) return;

			UpdateView(prevValue, value, true);
		}

		private void UpdateView(int prevValue, int value, bool animate = false)
		{
			if (animate)
			{
				StartCoroutine(animator.Animate(prevValue < value, value, mask));
				if (animatorElement != null)
				{
					animatorElement.SetActive(false);
					animatorElement.SetActive(true);
				}

				return;
			}

			valueTXT.text = string.Format(mask, value);
		}
	}
}