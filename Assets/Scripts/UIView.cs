﻿using System;
using UnityEngine;

namespace DiceGame
{
	public class UIView : MonoBehaviour
	{
		public bool disableOnAwake;
		public bool disableAfterHideAnimation;
		public bool destroyAfterHideAnimation;
		public Action<UIView> onShowAnimationFinished;
		public Action<UIView> onHideAnimationFinished;
		public Animator animator;
		private CanvasGroup canvasGroup;

		private bool isHiding;
		private bool needShow;

		public CanvasGroup CanvasGroup => canvasGroup;

		private static readonly int FADE_OUT = Animator.StringToHash("fadeOut");

		private void Awake()
		{
			canvasGroup = GetComponent<CanvasGroup>();
			if (disableOnAwake)
			{
				gameObject.SetActive(false);
			}
		}

		public void OnHideAnimationFinished()
		{
			if (destroyAfterHideAnimation)
			{
				Destroy(gameObject);
			}

			if (disableAfterHideAnimation)
			{
				gameObject.SetActive(false);
			}

			isHiding = false;

			onHideAnimationFinished?.Invoke(this);

			if (needShow)
			{
				Show();
			}
		}

		public void OnShowAnimationFinished()
		{
			onShowAnimationFinished?.Invoke(this);
		}

		public void Show()
		{
			if (gameObject.activeSelf && !isHiding) return;
			if (isHiding)
			{
				needShow = true;
				return;
			}

			needShow = false;
			if (canvasGroup != null)
			{
				canvasGroup.interactable = true;
			}

			gameObject.SetActive(true);
		}


		public void Hide()
		{
			if (canvasGroup != null)
			{
				canvasGroup.interactable = false;
			}

			if (isHiding && needShow)
			{
				needShow = false;
			}

			if (gameObject.activeSelf && !isHiding)
			{
				isHiding = true;
				animator.SetTrigger(FADE_OUT);
			}
		}

		public bool IsActive()
		{
			return gameObject.activeSelf || isHiding;
		}
	}
}