using TMPro;
using UnityEngine;

namespace DiceGame
{
	public class EnemyUIController : MonoBehaviour
	{
		[SerializeField] private TextMeshProUGUI healthTXT;
		[SerializeField] private TextMeshProUGUI defenceTXT;


		public void UpdateView(int defence, int currentHealth)
		{
			healthTXT.text = currentHealth.ToString();
			defenceTXT.text = defence.ToString();
		}
	}
}