using System.Collections.Generic;

namespace DiceGame
{
	public class ResultsUIView : ContainerUIView<ResultUIView>
	{
		public void UpdateView(List<int> results)
		{
			GetOrCreateItemViews(results.Count);
			for (int i = 0; i < results.Count; i++)
			{
				var result = results[i];
				var view = views[i];
				view.gameObject.SetActive(true);
				view.UpdateView(result);
			}
		}
	}
}