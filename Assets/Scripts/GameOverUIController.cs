using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DiceGame
{
	public class GameOverUIController : MonoBehaviour
	{
		[SerializeField] private Button restartBTN;
		[SerializeField] private GameObject root;

		private void Start()
		{
			restartBTN.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
			GameManager.Instance.OnStateChange.AddListener(x => UpdateView());
			UpdateView();
		}

		private void UpdateView()
		{
			root.SetActive(GameManager.Instance.CurrentState == GameState.GameOver);
		}
	}
}