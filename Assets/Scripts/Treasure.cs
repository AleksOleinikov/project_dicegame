using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DiceGame
{
	[CreateAssetMenu(menuName = "Dice Game/Treasure")]
	public class Treasure : ScriptableObject
	{
		[SerializeField] private int dropsCount;
		[SerializeField] private List<DropItem> dropItems;

		public List<ResourceToValue> GetReward()
		{
			var rewards = new List<ResourceToValue>();
			var possibleDrops = dropItems.ToList();

			for (int i = 0; i < dropsCount; i++)
			{
				var sum = possibleDrops.Sum(x => x.DropChance);

				var rnd = Random.Range(0, sum);
				var value = 0f;
				foreach (var drop in possibleDrops)
				{
					value += drop.DropChance;
					if (value >= rnd)
					{
						rewards.Add(new ResourceToValue(drop.Resource,drop.DropCount));
						possibleDrops.Remove(drop);
						break;
					}
				}
			}

			return rewards;
		}
	}

	[Serializable]
	public class DropItem
	{
		public Resource Resource;
		public int DropCount;
		public float DropChance;
	}
}