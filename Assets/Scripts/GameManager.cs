using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DiceGame
{
	public class GameManager : SingletonController<GameManager>
	{
		private const string HIGH_SCORE_KEY = "HighScore";

		[SerializeField] private Camera mainCamera;
		[SerializeField] private Player player;

		[SerializeField] private Resource rollResource;
		[SerializeField] private int startAvailableRolls = 20;
		[SerializeField] private Resource swordResource;
		[SerializeField] private int swordsForMoreDices = 3;
		[SerializeField] private int maxSwordsIfHasMaxSwords = 6;
		[SerializeField] private int maxDices = 5;
		[SerializeField] private Resource heartResource;
		[SerializeField] private int minHearts = 3;
		[SerializeField] private Resource cryResource;
		
		[Header("Battle")]
		[SerializeField] private UIView battlefield;
		[SerializeField] private GameObject battleCamera;
		[SerializeField] private int rollsOnDeathMinus = 1;

		[Header("Rewards")]
		[SerializeField] private Treasure treasureReward;
		[SerializeField] private Treasure rollsReward;
		[SerializeField] private Treasure crystalsReward;


		[Header("UI")]
		[SerializeField] private ResultsUIController resultView;

		[Header("Crystals")]
		[SerializeField] private int maxCrystals;
		[SerializeField] private int crystalsStepSize;
		[SerializeField] private CrystalsUIController crystalsView;

		public Camera MainCamera => mainCamera;

		private GameState currentState;

		public GameState CurrentState
		{
			get => currentState;
			set
			{
				currentState = value;
				OnStateChange?.Invoke(currentState);
			}
		}

		private int currentRollsAvailable;

		public int CurrentRollsAvailable
		{
			get => currentRollsAvailable;
			set
			{
				var prevValue = currentRollsAvailable;
				currentRollsAvailable = value;
				OnResoureValueChange?.Invoke(rollResource, prevValue, currentRollsAvailable);
			}
		}

		private int currentCrystals;

		public int CurrentCrystals
		{
			get => currentCrystals;
			set
			{
				var prevValue = currentCrystals;
				currentCrystals = value;
				OnResoureValueChange?.Invoke(cryResource, prevValue, currentCrystals);
			}
		}

		private int currentSwords;

		public int CurrentSwords
		{
			get => currentSwords;
			private set
			{
				var prevValue = currentSwords;

				if (value >= swordsForMoreDices && dc.ActiveDicesCount < maxDices)
				{
					currentSwords = 0;
					dc.AddDice();
				}
				else
				{
					currentSwords = Mathf.Min(value, maxSwordsIfHasMaxSwords);
				}

				OnResoureValueChange?.Invoke(swordResource, prevValue, currentSwords);
			}
		}

		private int currentHearts;

		public int CurrentHearts
		{
			get => currentHearts;
			set
			{
				var prevValue = currentHearts;
				currentHearts = value;
				OnResoureValueChange?.Invoke(heartResource, prevValue, currentHearts);
			}
		}

		private int currentFieldIndex;

		public int CurrentFieldIndex => currentFieldIndex;

		public int MinHearts => minHearts;

		public int MaxCrystals => maxCrystals;
		public int CrystalsStepSize => crystalsStepSize;

		public Player Player => player;

		public UnityEvent<GameState> OnStateChange { get; set; } = new UnityEvent<GameState>();
		public UnityEvent<Resource, int, int> OnResoureValueChange { get; set; } = new UnityEvent<Resource, int, int>();
		public UnityEvent OnScoreUpdate { get; set; } = new UnityEvent();


		public int HighScore { get; private set; }

		private bool isRollBlocked;


		//roll
		//move
		//rewards or fight
		//update map

		private BattleController bc;
		private DiceController dc;
		private MapGenerator mg;

		private void Start()
		{
			bc = BattleController.Instance;
			mg = MapGenerator.Instance;
			dc = DiceController.Instance;
			HighScore = PlayerPrefs.GetInt(HIGH_SCORE_KEY);
			ResetGame();
		}

		public void OnDicesResults(List<int> results, int finalResult)
		{
			switch (CurrentState)
			{
				case GameState.Game:
					StartCoroutine(Move(finalResult));
					currentFieldIndex += finalResult;
					break;
				case GameState.Battle:
					if (currentSwords > 0)
					{
						results.Add(currentSwords);
						finalResult += currentSwords;
					}

					bc.DoBattle(finalResult);
					break;
			}

			resultView.UpdateView(results, finalResult);
		}

		public void ResetGame()
		{
			player.MoveController.SetPosition(Vector3.zero);
			CurrentState = GameState.Game;
			CurrentRollsAvailable = startAvailableRolls;
			CurrentCrystals = 0;
			CurrentSwords = 0;
			CurrentHearts = minHearts;
			currentFieldIndex = 0;
			dc.Reset();
			mg.Reset();

			player.LookAt(mg.ActiveFieldsQueue.Peek().transform.position);
			CurrentState = GameState.Game;
		}

		public int GetCurrentResourceCount(Resource resource)
		{
			if (resource == cryResource)
			{
				return CurrentCrystals;
			}

			if (resource == swordResource)
			{
				return CurrentSwords;
			}

			if (resource == heartResource)
			{
				return CurrentHearts;
			}

			if (resource == rollResource)
			{
				return CurrentRollsAvailable;
			}

			return 0;
		}

		public void AddResource(Resource resource, int delta)
		{
			if (resource == cryResource)
			{
				CurrentCrystals += delta;
				return;
			}

			if (resource == swordResource)
			{
				CurrentSwords += delta;
				return;
			}

			if (resource == heartResource)
			{
				CurrentHearts += delta;
				return;
			}

			if (resource == rollResource)
			{
				CurrentRollsAvailable += delta;
				return;
			}
		}

		private IEnumerator Move(int steps)
		{
			player.AnimationController.Run();

			for (int i = 0; i < steps; i++)
			{
				mg.ChangeCurrentField(2);

				var targetPos = mg.CurrentField.transform.position;
				yield return StartCoroutine(player.MoveController.MoveSmooth(targetPos));

				player.LookAt(mg.ActiveFieldsQueue.Peek().transform.position);
			}

			player.AnimationController.Idle();

			UpdateScore();

			if (mg.CurrentField.CurrentType == FieldType.Reward)
			{
				yield return StartCoroutine(TreasureCoroutine(treasureReward));
			}
			else if (mg.CurrentField.CurrentType == FieldType.Rolls)
			{
				yield return StartCoroutine(TreasureCoroutine(rollsReward, false));
			}
			else if (mg.CurrentField.CurrentType == FieldType.Crystals)
			{
				yield return StartCoroutine(TreasureCoroutine(crystalsReward, false));
			}


			if (mg.CurrentField.CurrentType != FieldType.Enemy)
			{
				if (currentRollsAvailable <= 0)
				{
					GameOver();
					yield break;
				}

				mg.UpdateMap();
			}
			else
			{
				CurrentState = GameState.Battle;
				battleCamera.SetActive(true);
				battlefield.Show();
				bc.StartBattle();
			}

			UnblockRolls();
		}

		private IEnumerator TreasureCoroutine(Treasure treasure, bool showTreasure = true)
		{
			var rewards = treasure.GetReward();
			if (!showTreasure)
			{
				yield return StartCoroutine(
					TreasureController.Instance.OpeningCoroutineWithoutTreasure(mg.CurrentField.transform.position,
						rewards));
				yield break;
			}

			if (CurrentState != GameState.Battle)
			{
				yield return StartCoroutine(player.MoveController.MoveSmooth(mg.CurrentField.PlayerTreasurePos));

				player.LookAt(mg.CurrentField.TreasurePos);

				yield return StartCoroutine(
					TreasureController.Instance.OpeningCoroutine(mg.CurrentField.TreasurePos, rewards));
			}
			else
			{
				yield return StartCoroutine(
					TreasureController.Instance.OpeningCoroutine(mg.CurrentField.EnemyBattlePos, rewards));
			}


			yield return StartCoroutine(player.MoveController.MoveSmooth(mg.CurrentField.transform.position));
			player.LookAt(mg.ActiveFieldsQueue.Peek().transform.position);
		}

		public void RollDices()
		{
			if (isRollBlocked) return;

			isRollBlocked = true;
			resultView.Hide();

			dc.RollDices();
			if (CurrentState != GameState.Game) return;

			CurrentRollsAvailable--;
		}


		public IEnumerator OnBattleEnd(bool isVictory, Treasure reward)
		{
			if (isVictory)
			{
				yield return StartCoroutine(TreasureCoroutine(reward));
			}
			else
			{
				StartCoroutine(player.MoveController.MoveSmooth(mg.CurrentField.transform.position));
				CurrentRollsAvailable -= rollsOnDeathMinus;
			}

			player.PlayerUI.OnBattleEnd(isVictory);
			player.LookAt(mg.ActiveFieldsQueue.Peek().transform.position);
			battleCamera.SetActive(false);
			battlefield.Hide();

			if (currentRollsAvailable <= 0)
			{
				GameOver();
				yield break;
			}

			mg.UpdateMap();

			CurrentState = GameState.Game;
			UnblockRolls();

			if (CurrentHearts >= MinHearts) yield break;

			CurrentHearts = MinHearts;
		}

		private void GameOver()
		{
			player.AnimationController.Death();
			CurrentState = GameState.GameOver;
			PlayerPrefs.SetInt(HIGH_SCORE_KEY, HighScore);
			PlayerPrefs.Save();
		}

		private void UpdateScore()
		{
			HighScore = Mathf.Max(HighScore, CurrentFieldIndex);
			OnScoreUpdate?.Invoke();
		}

		public void UnblockRolls()
		{
			isRollBlocked = false;
		}
	}
}