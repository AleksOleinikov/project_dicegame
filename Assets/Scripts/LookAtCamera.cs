using UnityEngine;

namespace DiceGame
{
	public class LookAtCamera : MonoBehaviour
	{
		[SerializeField] private bool ignoreX;
		[SerializeField] private bool ignoreY;
		[SerializeField] private bool ignoreZ;

		private Camera mainCamera;

		private void Start()
		{
			mainCamera = GameManager.Instance.MainCamera;
		}

		private void Update()
		{
			var mainCamPos = mainCamera.transform.position;
			var currentPos = transform.position;
			var targetPos = new Vector3(ignoreX ? currentPos.x : mainCamPos.x, ignoreY ? currentPos.y : mainCamPos.y,
				ignoreZ ? currentPos.z : mainCamPos.z);
			transform.LookAt(targetPos);
		}
	}
}