using System;
using UnityEngine;

namespace DiceGame
{
	[CreateAssetMenu(menuName = "Dice Game/Resource")]
	public class Resource : ScriptableObject
	{
		public string Name;
		public Sprite Icon;
	}

	[Serializable]
	public class ResourceToValue
	{
		public Resource Resource;
		public int Value;

		public ResourceToValue(Resource resource, int value)
		{
			Resource = resource;
			Value = value;
		}
	}
}