using UnityEngine;

namespace DiceGame
{
	public class DiceSideProvider : MonoBehaviour
	{
		[SerializeField] private DiceSide side;

		public DiceSide Side => side;
		public Vector3 Position => transform.position;
	}
}