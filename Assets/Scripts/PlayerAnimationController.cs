using UnityEngine;

namespace DiceGame
{
	public class PlayerAnimationController : MonoBehaviour
	{
		[SerializeField] private Animator animator;
		[SerializeField] private int attackTypes = 3;

		private static readonly int fallId = Animator.StringToHash("Fall");
		private static readonly int idleId = Animator.StringToHash("Idle");
		private static readonly int attackId = Animator.StringToHash("Attack");
		private static readonly int attackTypeId = Animator.StringToHash("AttackType");
		private static readonly int runId = Animator.StringToHash("Run");
		private static readonly int deathId = Animator.StringToHash("Death");

		public void DoAttack()
		{
			var attackTypeIndex = Random.Range(0, attackTypes);
			animator.SetInteger(attackTypeId, attackTypeIndex);
			animator.SetTrigger(attackId);
		}

		public void Fall()
		{
			animator.SetTrigger(fallId);
		}

		public void Idle()
		{
			animator.SetTrigger(idleId);
		}

		public void Run()
		{
			animator.SetTrigger(runId);
		}

		public void Death()
		{
			animator.SetTrigger(deathId);
		}
	}
}