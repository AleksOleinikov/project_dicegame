using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DiceGame
{
	public class DiceController : SingletonController<DiceController>
	{
		[Header("Dice")]
		[SerializeField] private Dice dicePrefab;
		[SerializeField] private int startDiceAmount = 2;
		[SerializeField] private float maxXZRangeOffset = 2f;
		[SerializeField] private float maxYRangeOffset = .2f;
		[SerializeField] private float rollHeight = 5f;
		[SerializeField] private float maxRollUpForce = 15;
		[SerializeField] private float maxRollHorizontalForce = 10;
		[SerializeField] private float maxRollTorque = 5;

		public int ActiveDicesCount => activeDices.Count;

		private Stack<Dice> diceStack;
		private Stack<Dice> activeDices;
		private GameObject dicePoolRoot;
		private List<int> results = new List<int>();

		protected void Awake()
		{
			base.Awake();

			diceStack = new Stack<Dice>(startDiceAmount);
			activeDices = new Stack<Dice>(startDiceAmount);
			dicePoolRoot = new GameObject("DicePoolRoot");
			dicePoolRoot.transform.parent = transform;
		}

		public void Update()
		{
			if (Input.GetKeyDown(KeyCode.L))
			{
				RollDices();
			}

			if (Input.GetKeyDown(KeyCode.S))
			{
				AddDice();
			}

			if (Input.GetKeyDown(KeyCode.A))
			{
				RemoveDice();
			}
		}

		public void RollDices()
		{
			results.Clear();

			foreach (var dice in activeDices)
			{
				var position = new Vector3(Random.Range(-maxXZRangeOffset, maxXZRangeOffset),
					Random.Range(rollHeight - maxXZRangeOffset, rollHeight + maxYRangeOffset),
					Random.Range(-maxXZRangeOffset, maxXZRangeOffset));
				var rotation = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
				dice.gameObject.SetActive(true);
				dice.transform.localPosition = position;
				dice.transform.rotation = Quaternion.Euler(rotation);
				dice.Rigidbody.AddForce(Vector3.up * Random.Range(-maxRollUpForce, maxRollUpForce) +
				                        Vector3.forward * Random.Range(-maxRollHorizontalForce,
					                        maxRollHorizontalForce) +
				                        Vector3.right * Random.Range(-maxRollHorizontalForce, maxRollHorizontalForce));
				dice.Rigidbody.AddTorque(Random.Range(-maxRollTorque, maxRollTorque),
					Random.Range(-maxRollTorque, maxRollTorque), Random.Range(-maxRollTorque, maxRollTorque));
				dice.StarRolling();
			}
		}

		public void Reset()
		{
			var dicesDif = activeDices.Count - startDiceAmount;
			if (dicesDif == 0) return;

			if (dicesDif < 0)
			{
				AddDice(-dicesDif);
			}
			else
			{
				for (int i = 0; i < dicesDif; i++)
				{
					RemoveDice();
				}
			}
		}

		public void AddResult(int result)
		{
			results.Add(result);
			if (results.Count != ActiveDicesCount) return;

			GameManager.Instance.OnDicesResults(results, results.Sum());
		}

		public void AddDice(int count = 1)
		{
			for (int i = 0; i < count; i++)
			{
				if (diceStack.Count <= 0)
				{
					InstantiateDice();
				}

				var dice = diceStack.Pop();
				dice.gameObject.SetActive(false);
				activeDices.Push(dice);
			}
		}

		private void RemoveDice()
		{
			if (activeDices.Count <= 0) return;

			var dice = activeDices.Pop();
			dice.gameObject.SetActive(false);
			diceStack.Push(dice);
		}

		private void InstantiateDice()
		{
			var instance = Instantiate(dicePrefab, dicePoolRoot.transform);
			instance.gameObject.SetActive(false);
			diceStack.Push(instance);
		}
	}
}